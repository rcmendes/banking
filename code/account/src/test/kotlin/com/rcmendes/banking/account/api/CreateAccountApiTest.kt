package com.rcmendes.banking.account.api

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.rcmendes.banking.account.AccountInfoResponse
import com.rcmendes.banking.account.Application
import com.rcmendes.banking.account.CreateAccountRequest
import io.restassured.RestAssured.given
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.notNullValue
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.math.BigDecimal
import java.util.concurrent.ConcurrentLinkedDeque
import kotlin.test.assertEquals

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CreateAccountApiTest {
    private val jsonParser = jacksonObjectMapper()

    @BeforeAll
    fun beforeAll() {
        Application.init()
    }

    @AfterAll
    fun afterAll() {
        Application.shutdown()
    }

    private fun createAccount(owner: String, amount: BigDecimal = BigDecimal.ZERO, limit: BigDecimal = BigDecimal.ZERO): String {

        val request = CreateAccountRequest(owner, amount, limit)

        val jsonAsString = jsonParser.writeValueAsString(request)

        val response = given()
            .contentType("application/json")
            .body(jsonAsString)
            .`when`()
            .post("/api/v1/accounts")
            .then()
            .statusCode(201)
            .body("id", notNullValue())
            .body("owner", equalTo(owner))
            .extract()

        val responseBalance = response.path<Any>("balance").toString().toBigDecimal()
        val responseLimit = response.path<Any>("limit").toString().toBigDecimal()

        assertEquals(amount, responseBalance)
        assertEquals(limit, responseLimit)

        return response.path<String>("id")
    }

    @Test
    fun `create account with initial funds and limit`() {
        val amount = BigDecimal(50.0)
        val owner = "Owner 1"
        val limit = BigDecimal(10.0)

        createAccount(owner, amount, limit)
    }

    @Test
    fun `creating account | invalid inputs - no owner`() {
        var request = CreateAccountRequest("    ", BigDecimal.ZERO, BigDecimal(-10.0))

        val jsonAsString = jsonParser.writeValueAsString(request)

        given()
            .contentType("application/json")
            .body(jsonAsString)
            .`when`()
            .post("/api/v1/accounts")
            .then()
            .statusCode(400)
    }

    @Test
    fun `creating account | invalid inputs - negative amount`() {
        var request = CreateAccountRequest("Owner", BigDecimal(-10.0))

        val jsonAsString = jsonParser.writeValueAsString(request)

        given()
            .contentType("application/json")
            .body(jsonAsString)
            .`when`()
            .post("/api/v1/accounts")
            .then()
            .statusCode(422)
            .body("message", notNullValue())
    }

    @Test
    fun `creating account | invalid inputs - negative limit`() {
        var request = CreateAccountRequest("Owner", BigDecimal.ZERO, BigDecimal(-10.0))

        val jsonAsString = jsonParser.writeValueAsString(request)

        given()
            .contentType("application/json")
            .body(jsonAsString)
            .`when`()
            .post("/api/v1/accounts")
            .then()
            .statusCode(422)
            .body("message", notNullValue())
    }

    @Test
    fun `creating account | invalid body data`() {
        given()
            .contentType("application/json")
            .body("{\"xxx\":\"yyy\"}")
            .`when`()
            .post("/api/v1/accounts")
            .then()
            .statusCode(400)
    }

    @Test
    fun `create accounts | concurrence`() = runBlocking {
        val jobs = ConcurrentLinkedDeque<Job>()
        repeat(100) { i ->
            val job = GlobalScope.launch {
                createAccount("Owner $i", i.toBigDecimal(), BigDecimal(i * 10.0))
            }
            jobs.add(job)
        }

        jobs.forEach {
            it.join()
        }

        val response = given()
            .contentType("application/json")
            .`when`()
            .get("/api/v1/accounts")
            .then()
            .statusCode(200)
            .extract()

        val list = response.jsonPath().getList<AccountInfoResponse>("$")

        // If test is run isolated, result must be 100
        assert(list.size >= 100)
    }
}