package com.rcmendes.banking.account.api

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.rcmendes.banking.account.TransferMoneyIntoAccountsRequest
import com.rcmendes.banking.account.Application
import com.rcmendes.banking.account.CreateAccountRequest
import io.restassured.RestAssured.given
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.notNullValue
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.math.BigDecimal
import kotlin.test.assertEquals

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TransferIntoAccountsApiTest {
    private val jsonParser = jacksonObjectMapper()

    @BeforeAll
    fun beforeAll() {
        Application.init()
    }

    @AfterAll
    fun afterAll() {
        Application.shutdown()
    }

    private fun createAccount(owner: String, amount: BigDecimal = BigDecimal.ZERO, limit: BigDecimal = BigDecimal.ZERO): String {
        val request = CreateAccountRequest(owner, amount, limit)

        val jsonAsString = jsonParser.writeValueAsString(request)

        val response = given()
            .contentType("application/json")
            .body(jsonAsString)
            .`when`()
            .post("/api/v1/accounts")
            .then()
            .statusCode(201)
            .body("id", notNullValue())
            .body("owner", equalTo(owner))
            .extract()

        val responseBalance = response.path<Any>("balance").toString().toBigDecimal()
        val responseLimit = response.path<Any>("limit").toString().toBigDecimal()

        assertEquals(amount, responseBalance)
        assertEquals(limit, responseLimit)

        return response.path<String>("id")
    }

    @Test
    fun `transfer money with funds and limit in the source Account`() {
        val amount = BigDecimal(50.0)
        val owner1 = "Owner 1"
        val owner2 = "Owner 2"
        val balance1 = BigDecimal(1000.0)
        val balance2 = BigDecimal.ZERO
        val limit1 = BigDecimal(10.0)
        val limit2 = BigDecimal(10.0)

        val fromAccountId = createAccount(owner1, balance1, limit1)
        val toAccountId = createAccount(owner2, balance2, limit2)


        val request =
            TransferMoneyIntoAccountsRequest(amount = amount, toAccountId = toAccountId)

        val jsonAsString = jsonParser.writeValueAsString(request)

        val response = given()
            .pathParam("fromId", fromAccountId)
            .contentType("application/json")
            .body(jsonAsString)
            .`when`()
            .post("/api/v1/accounts/{fromId}/transfers")
            .then()
            .statusCode(200)
            .body("id", equalTo(fromAccountId))
            .extract()

        val responseBalance = response.path<Any>("balance").toString().toBigDecimal()
        assertEquals(balance1 - amount, responseBalance)
    }

    @Test
    fun `transfer money exceeding balance but covered by limit of the source Account`() {
        val amount = BigDecimal(1050.0)
        val owner1 = "Owner 1"
        val owner2 = "Owner 2"
        val balance1 = BigDecimal(1000.0)
        val balance2 = BigDecimal.ZERO
        val limit1 = BigDecimal(100.0)
        val limit2 = BigDecimal(10.0)

        val fromAccountId = createAccount(owner1, balance1, limit1)
        val toAccountId = createAccount(owner2, balance2, limit2)

        val request =
            TransferMoneyIntoAccountsRequest(amount = amount, toAccountId = toAccountId)

        val jsonAsString = jsonParser.writeValueAsString(request)

        val response = given()
            .pathParam("fromId", fromAccountId)
            .contentType("application/json")
            .body(jsonAsString)
            .`when`()
            .post("/api/v1/accounts/{fromId}/transfers")
            .then()
            .statusCode(200)
            .body("id", equalTo(fromAccountId))
            .extract()

        val responseBalance = response.path<Any>("balance").toString().toBigDecimal()
        assertEquals(balance1 - amount, responseBalance)
    }

    @Test
    fun `transfer money exceeding balance + limit of the source Account`() {
        val amount = BigDecimal(1500.0)
        val owner1 = "Owner 1"
        val owner2 = "Owner 2"
        val balance1 = BigDecimal(1000.0)
        val balance2 = BigDecimal(0.0)
        val limit1 = BigDecimal(10.0)
        val limit2 = BigDecimal(10.0)

        val fromAccountId = createAccount(owner1, balance1, limit1)
        val toAccountId = createAccount(owner2, balance2, limit2)

        val request =
            TransferMoneyIntoAccountsRequest(amount = amount, toAccountId = toAccountId)

        val jsonAsString = jsonParser.writeValueAsString(request)

        given()
            .pathParam("fromId", fromAccountId)
            .contentType("application/json")
            .body(jsonAsString)
            .`when`()
            .post("/api/v1/accounts/{fromId}/transfers")
            .then()
            .statusCode(422)
            .body("message", notNullValue())
    }

    @Test
    fun `transfer negative amount`() {
        val amount = BigDecimal(-500.0)
        val owner1 = "Owner 1"
        val owner2 = "Owner 2"
        val balance1 = BigDecimal(1000.0)
        val balance2 = BigDecimal.ZERO
        val limit1 = BigDecimal(10.0)
        val limit2 = BigDecimal(10.0)

        val fromAccountId = createAccount(owner1, balance1, limit1)
        val toAccountId = createAccount(owner2, balance2, limit2)

        val request =
            TransferMoneyIntoAccountsRequest(amount = amount, toAccountId = toAccountId)

        val jsonAsString = jsonParser.writeValueAsString(request)

        given()
            .pathParam("fromId", fromAccountId)
            .contentType("application/json")
            .body(jsonAsString)
            .`when`()
            .post("/api/v1/accounts/{fromId}/transfers")
            .then()
            .statusCode(422)
            .body("message", notNullValue())
    }

    @Test
    fun `transfer into accounts | invalid source account number`() {
        val amount = BigDecimal(1500.0)
        val owner1 = "Owner 1"
        val owner2 = "Owner 2"
        val balance1 = BigDecimal(1000.0)
        val balance2 = BigDecimal.ZERO
        val limit1 = BigDecimal(10.0)
        val limit2 = BigDecimal(10.0)

        val fromAccountId = createAccount(owner1, balance1, limit1)
        val toAccountId = createAccount(owner2, balance2, limit2)

        val request =
            TransferMoneyIntoAccountsRequest(amount = amount, toAccountId = toAccountId)

        val jsonAsString = jsonParser.writeValueAsString(request)

        given()
            .pathParam("fromId", fromAccountId.substring(5))
            .contentType("application/json")
            .body(jsonAsString)
            .`when`()
            .post("/api/v1/accounts/{fromId}/transfers")
            .then()
            .statusCode(422)
            .body("message", notNullValue())
    }

    @Test
    fun `transfer into accounts | invalid body data`() {
        val owner = "Owner 1"
        val balance = BigDecimal(1000.0)
        val limit = BigDecimal(10.0)

        val fromAccountId = createAccount(owner, balance, limit)

        val jsonAsString = jsonParser.writeValueAsString("-----")

        given()
            .pathParam("id", fromAccountId)
            .contentType("application/json")
            .body(jsonAsString)
            .`when`()
            .post("/api/v1/accounts/{id}/transfers")
            .then()
            .statusCode(400)
    }

    @Test
    fun `transfer into accounts | concurrence | consistence test`() = runBlocking {
        val owner1 = "Owner 1"
        val owner2 = "Owner 2"
        val balance1 = BigDecimal.ZERO
        val balance2 = BigDecimal.ZERO
        val limit1 = BigDecimal(1000000.0)
        val limit2 = BigDecimal(1000000.0)

        val fromAccountId = createAccount(owner1, balance1, limit1)
        val toAccountId = createAccount(owner2, balance2, limit2)

        val jobFromIdToIdTransfer = GlobalScope.launch {
            repeat(1000) {
                val amount = BigDecimal(1.0)
                transfer(fromAccountId, amount, toAccountId)
            }
        }

        val jobToIdFromIdTransfer = GlobalScope.launch {
            repeat(1000) {
                val amount = BigDecimal(1.0)
                transfer(toAccountId, amount, fromAccountId)
            }
        }

        jobFromIdToIdTransfer.join()
        jobToIdFromIdTransfer.join()

        val response1 = given()
            .pathParam("id", fromAccountId)
            .contentType("application/json")
            .`when`()
            .get("/api/v1/accounts/{id}")
            .then()
            .statusCode(200)
            .extract()

        val responseBalance1 = response1.path<Any>("balance").toString().toBigDecimal()
        val responseLimit1 = response1.path<Any>("limit").toString().toBigDecimal()

        assertEquals(balance1, responseBalance1)
        assertEquals(limit1, responseLimit1)

        val response2 = given()
            .pathParam("id", toAccountId)
            .contentType("application/json")
            .`when`()
            .get("/api/v1/accounts/{id}")
            .then()
            .statusCode(200)
            .extract()

        val responseBalance2 = response2.path<Any>("balance").toString().toBigDecimal()
        val responseLimit2 = response2.path<Any>("limit").toString().toBigDecimal()

        assertEquals(balance2, responseBalance2)
        assertEquals(limit2, responseLimit2)
    }

    private fun transfer(fromId: String, amount: BigDecimal, toId: String) {
        val request =
            TransferMoneyIntoAccountsRequest(amount = amount, toAccountId = toId)

        val jsonAsString = jsonParser.writeValueAsString(request)

        given()
            .pathParam("fromId", fromId)
            .contentType("application/json")
            .body(jsonAsString)
            .`when`()
            .post("/api/v1/accounts/{fromId}/transfers")
            .then()
            .statusCode(200)
    }
}