package com.rcmendes.banking.account.models

import com.rcmendes.banking.account.Account
import com.rcmendes.banking.account.InsufficientFundsError
import com.rcmendes.banking.account.InvalidAmountValueError
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.function.Executable
import java.math.BigDecimal
import java.util.*
import kotlin.test.assertEquals

class AccountTest {
    @Test
    fun `deposit positive amount into account`() {
        val balance = BigDecimal(100.0)
        val amount = BigDecimal(50.0)
        val account = Account("Account Owner 1", balance = balance, limit = BigDecimal.ZERO)
        account.id = UUID.randomUUID()

        account.deposit(amount)

        assertEquals(balance + amount, account.balance)
    }

    @Test
    fun `deposit negative or zero amount into account`() {
        val balance = BigDecimal(100.0)
        val account = Account("Account Owner 1", balance = balance, limit = BigDecimal.ZERO)
        account.id = UUID.randomUUID()

        Assertions.assertAll(
            Executable {
                assertThrows<InvalidAmountValueError> {
                    account.deposit(BigDecimal.ZERO)
                }
            },
            Executable {
                assertThrows<InvalidAmountValueError> {
                    account.deposit(BigDecimal(-100.0))
                }
            }
        )
    }

    @Test
    fun `withdraw amount into account having funds and no limit`() {
        val balance = BigDecimal(100.0)
        val amount = BigDecimal(50.0)
        val account = Account("Account Owner 1", balance = balance, limit = BigDecimal.ZERO)
        account.id = UUID.randomUUID()

        account.withdraw(amount)
        assertEquals(balance - amount, account.balance)
    }

    @Test
    fun `withdraw amount into account having no funds but having limit`() {
        val balance = BigDecimal(0.0)
        val amount = BigDecimal(50.0)
        val limit = BigDecimal(100.0)
        val account = Account("Account Owner 1", balance = balance, limit = limit)
        account.id = UUID.randomUUID()

        account.withdraw(amount)
        assertEquals(balance - amount, account.balance)
    }

    @Test
    fun `withdraw amount into account having no funds and no limit`() {
        val balance = BigDecimal(0.0)
        val amount = BigDecimal(100.0)
        val limit = BigDecimal(0.0)
        val account = Account("Account Owner 1", balance = balance, limit = limit)
        account.id = UUID.randomUUID()

        assertThrows<InsufficientFundsError> {
            account.withdraw(amount)
        }
    }
}