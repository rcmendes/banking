package com.rcmendes.banking.account.api

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.rcmendes.banking.account.AccountInfoResponse
import com.rcmendes.banking.account.Application
import com.rcmendes.banking.account.CreateAccountRequest
import io.restassured.RestAssured.given
import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.notNullValue
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.math.BigDecimal
import kotlin.test.assertEquals

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AccountInfoApiTest {
    private val jsonParser = jacksonObjectMapper()

    @BeforeAll
    fun beforeAll() {
        Application.init()
    }

    @AfterAll
    fun afterAll() {
        Application.shutdown()
    }

    private fun createAccount(owner: String, amount: BigDecimal= BigDecimal.ZERO, limit: BigDecimal = BigDecimal.ZERO): String {
        val request = CreateAccountRequest(owner, amount, limit)

        val jsonAsString = jsonParser.writeValueAsString(request)

        val response = given()
            .contentType("application/json")
            .body(jsonAsString)
            .`when`()
            .post("/api/v1/accounts")
            .then()
            .statusCode(201)
            .body("id", notNullValue())
            .body("owner", equalTo(owner))
            .extract()

        val responseBalance = response.path<Any>("balance").toString().toBigDecimal()
        val responseLimit = response.path<Any>("limit").toString().toBigDecimal()

        assertEquals(amount, responseBalance)
        assertEquals(limit, responseLimit)

        return response.path<String>("id")
    }

    @Test
    fun `account info | get info from one account`() {
        val owner = "Owner 1"
        val balance = BigDecimal(1000.0)
        val limit = BigDecimal(25.0)
        val accountId = createAccount(owner, balance, limit)

        val response = given()
            .pathParam("id", accountId)
            .contentType("application/json")
            .`when`()
            .get("/api/v1/accounts/{id}")
            .then()
            .statusCode(200)
            .body("id", notNullValue())
            .body("owner", equalTo(owner))
            .extract()

        val responseBalance = response.path<Any>("balance").toString().toBigDecimal()
        val responseLimit = response.path<Any>("limit").toString().toBigDecimal()

        assertEquals(balance, responseBalance)
        assertEquals(limit, responseLimit)
    }

    @Test
    fun `account info | get all accounts`() {
        for (i in 1..10) {
            createAccount("Owner $i", BigDecimal(i * 1000.0), BigDecimal(i * 25.0))
        }

        val response = given()
            .contentType("application/json")
            .`when`()
            .get("/api/v1/accounts")
            .then()
            .statusCode(200)
            .extract()

        val list = response.jsonPath().getList<AccountInfoResponse>("$")

        // If test is run isolated, result must be 10
        assert(list.size >= 10)
    }

    @Test
    fun `account info | invalid account number`() {
        val owner = "Owner 1"
        val balance = BigDecimal(1000.0)
        val limit = BigDecimal(25.0)
        val accountId = createAccount(owner, balance, limit)

        val response = given()
            .pathParam("id", accountId)
            .contentType("application/json")
            .`when`()
            .get("/api/v1/accounts/{id}")
            .then()
            .statusCode(200)
            .body("id", notNullValue())
            .body("owner", equalTo(owner))
            .extract()

        val responseBalance = response.path<Any>("balance").toString().toBigDecimal()
        val responseLimit = response.path<Any>("limit").toString().toBigDecimal()

        assertEquals(balance, responseBalance)
        assertEquals(limit, responseLimit)
    }
}