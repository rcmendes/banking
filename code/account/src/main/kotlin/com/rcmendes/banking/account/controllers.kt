package com.rcmendes.banking.account

import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.*
import io.javalin.http.Context
import io.javalin.plugin.openapi.dsl.OpenApiDocumentation
import io.javalin.plugin.openapi.dsl.documented
import org.slf4j.LoggerFactory

/**
 * Manages all services for Account Management by providing them as resources (endpoints).
 * @param accountService Account service manager.
 * @param server Server which will handle all services as resources (endpoints).
 */
class AccountController(private val accountService: AccountService, private val server: Javalin) {
    private val log = LoggerFactory.getLogger(this::class.qualifiedName)

    /**
     * Initialize the services routes and sets the documentation of the /accounts API.
     */
    init {
        val createAccountDoc = buildCreateAccountDocumentation()
        val transferDoc = buildTransferIntoAccountsDocumentation()
        val accountInfoDoc = buildAccountInfoDocumentation()
        val allAccountsInfoDoc = buildAllAccountsInfoDocumentation()


        with(server) {
            routes {
                path("api") {
                    path("v1") {
                        path("accounts") {
                            get(documented(allAccountsInfoDoc) { ctx -> listAllAccountsHandler(ctx) })
                            post(documented(createAccountDoc) { ctx -> createAccountHandler(ctx) })
                            path(":id") {
                                get(documented(accountInfoDoc) { ctx -> getAccountByIdHandler(ctx) })
                                path("transfers") {
                                    post(documented(transferDoc) { ctx -> transferHandler(ctx) })
                                }
                            }
                        }
                    }
                }
            }
        }

    }

    private fun transferHandler(ctx: Context) =
        responseHandler<AccountBalanceResponse>(ctx) {
            val validator = ctx.bodyValidator<TransferMoneyIntoAccountsRequest>()

            validator.check({
                it.toAccountId.isNotBlank()
            }, "Target Account ID must be not blank")

            val transfer = validator.get()

            transfer.fromAccountId =
                ctx.pathParam<String>("id").check({
                    it.isNotBlank()
                }, "Source Account ID must be not blank").get()

            accountService.transferIntoAccounts(transfer)
        }

    private fun createAccountHandler(ctx: Context) = responseHandler<AccountInfoResponse>(ctx) {
        val validator = ctx.bodyValidator<CreateAccountRequest>()

        validator.check({
            it.owner.isNotBlank()
        }, "Owner must be not blank")

        val createAccount = validator.get()

        ctx.status(201)
        accountService.createAccount(createAccount)
    }

    private fun listAllAccountsHandler(ctx: Context) = responseHandler<List<AccountInfoResponse>>(ctx) {
        accountService.listAllAccounts()
    }

    private fun getAccountByIdHandler(ctx: Context) = responseHandler<AccountInfoResponse>(ctx) {
        val id = ctx.pathParam<String>("id").check({
            it.isNotBlank()
        }, "Account ID must be not blank").get()
        accountService.getAccountById(id)
    }

    private fun <R : Any> responseHandler(ctx: Context, fn: (ctx: Context) -> R?) = try {
        fn(ctx)?.let {
            ctx.json(it as Any)
        }
    } catch (ex: BusinessException) {
        log.info(ex.toString())
        ctx.status(422)
        ctx.json(BusinessErrorResponse(ex.message))
    } catch (ex: com.fasterxml.jackson.databind.exc.MismatchedInputException) {
        log.info(ex.toString())
        ctx.status(400)
    } catch (ex: Exception) {
        log.info(ex.toString())
        ctx.status(400)
    }


    private fun buildCreateAccountDocumentation(): OpenApiDocumentation {
        return OpenApiDocumentation()
            .operation { openApiOperation ->
                openApiOperation.operationId("createAccount")
                openApiOperation.summary("Create a bank account")
                openApiOperation.deprecated(false)
                openApiOperation.addTagsItem("accounts")
            }

            //Body
            .body<CreateAccountRequest>()

            // Responses
            .json<AccountInfoResponse>("201")
            .result<NoContent>("400")
            .result<BusinessException>("422")
    }

    private fun buildTransferIntoAccountsDocumentation(): OpenApiDocumentation {
        return OpenApiDocumentation()
            .operation { openApiOperation ->
                openApiOperation.operationId("transferIntoAccounts")
                openApiOperation.summary("Transfer an amount of money into bank accounts")
                openApiOperation.deprecated(false)
                openApiOperation.addTagsItem("accounts")
            }

            // Parameters
            .pathParam<String>("id") { openApiParam ->
                openApiParam.description("ID (number) of source account")
            }

            // Body
            .body<TransferMoneyIntoAccountsRequest>()

            // Responses
            .json<AccountBalanceResponse>("200")
            .result<NoContent>("400")
            .result<BusinessException>("422")
    }

    private fun buildAccountInfoDocumentation(): OpenApiDocumentation {
        return OpenApiDocumentation()
            .operation { openApiOperation ->
                openApiOperation.operationId("accountInfo")
                openApiOperation.summary("Show general information about the specified account")
                openApiOperation.deprecated(false)
                openApiOperation.addTagsItem("accounts")
            }

            // Parameters
            .pathParam<String>("id") { openApiParam ->
                openApiParam.description("ID (number) of the account")
            }

            // Body
            .body<TransferMoneyIntoAccountsRequest>()

            // Responses
            .json<AccountInfoResponse>("200")
            .result<NoContent>("400")
            .result<BusinessException>("422")
    }

    private fun buildAllAccountsInfoDocumentation(): OpenApiDocumentation {
        return OpenApiDocumentation()
            .operation { openApiOperation ->
                openApiOperation.operationId("allAccountsInfo")
                openApiOperation.summary("Show general information about all the accounts")
                openApiOperation.deprecated(false)
                openApiOperation.addTagsItem("accounts")
            }

            // Responses
            .jsonArray<AccountInfoResponse>("200")
    }

    class NoContent
}