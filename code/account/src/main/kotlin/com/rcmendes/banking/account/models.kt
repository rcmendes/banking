package com.rcmendes.banking.account

import java.math.BigDecimal
import java.util.*

/**
 * Represents an account.
 */
data class Account(
    var owner: String,
    var balance: BigDecimal = BigDecimal.ZERO,
    var limit: BigDecimal = BigDecimal.ZERO
) {
    lateinit var id: UUID

    /**
     * Deposits a amount into account.
     * @param amount Amount to be deposited.
     */
    fun deposit(amount: BigDecimal) {
        if (amount <= BigDecimal.ZERO) {
            throw InvalidAmountValueError()
        }

        this.balance = this.balance.add(amount)
    }

    /**
     * Withdraw a amount into account.
     * @param amount Amount to be withdrawn.
     */
    fun withdraw(amount: BigDecimal) {
        if (amount <= BigDecimal.ZERO) {
            throw InvalidAmountValueError()
        }

        if (balance + limit < amount) {
            throw  InsufficientFundsError(this.id.toString())
        }

        this.balance = this.balance.subtract(amount)
    }
}