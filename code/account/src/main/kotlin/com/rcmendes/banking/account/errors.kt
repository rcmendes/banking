package com.rcmendes.banking.account

sealed class BusinessException(override val message: String) :
    Exception(message)

/**
 * Business error to be thrown when the provided account ID (number) was not found.
 */
class AccountNotFoundError(private val id: String) : BusinessException("Account number '$id' was not found")

/**
 * Business error to be thrown when the account does not have sufficient funds.
 */
class InsufficientFundsError(private val id: String) :
    BusinessException("There is no sufficient funds for this transaction into account number '$id'")

/**
 * Business error to be thrown when a negative value is provided instead of zero or a positive value.
 */
class NegativeValueError(private val property: String) :
    BusinessException("The $property must be zero or a positive value")

/**
 * Business error to be thrown when the specified amount is not a positive value.
 */
class InvalidAmountValueError() :
    BusinessException("Amount must be a positive value")

/**
 * Business error to be thrown when the provided data (String) does not have a minimum characters length.
 */
class InsufficientCharactersLengthError(private val property: String, private val minimum: Int) :
    BusinessException("The $property must have at least $minimum characters")

/**
 * Business error to be thrown when the provided account ID (number) is invalid.
 */
class InvalidAccountIdError() : BusinessException("The Account ID is invalid")