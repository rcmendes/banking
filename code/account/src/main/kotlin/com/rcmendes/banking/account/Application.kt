package com.rcmendes.banking.account

import io.javalin.Javalin
import io.javalin.plugin.openapi.OpenApiOptions
import io.javalin.plugin.openapi.OpenApiPlugin
import io.javalin.plugin.openapi.ui.SwaggerOptions
import io.swagger.v3.oas.models.info.Info
import org.slf4j.LoggerFactory


fun main(args: Array<String>) {
    val argument = "port="

    var port: Int? = null
    args.filter {
        it.contains(argument)
    }.forEach {
        val portAsString = it.substringAfter(argument)
        port = portAsString.toIntOrNull()
    }

    if (port != null) {
        Application.init(port!!)
    } else {
        Application.init()
    }
}

/**
 * Class representing the Application. Responsible for set parameters and init the server.
 */
object Application {
    private val log = LoggerFactory.getLogger(this::class.java)
    private lateinit var server: Javalin

    /**
     * Initializes server and sets application parameters.
     * @param port Port which the server will be run. Default value 8080.
     */
    fun init(port: Int = 8080) {
        server = Javalin.create { config ->
            config.defaultContentType = "application/json"
            config.enableCorsForAllOrigins()
            config.registerPlugin(OpenApiPlugin(getOpenApiOptions()));
        }

        initFilters()
        initExceptionHandlers()

        AccountController(AccountService(AccountRepositoryInMemory), server)

        server.start(port)
    }

    /**
     * Shutdown the application server.
     */
    fun shutdown() {
        server.stop()
    }

    private fun initFilters() {
        with(server) {
            before { ctx ->
                val requestLog =
                    "${ctx.protocol()}\t${ctx.method()} \t${ctx.host()}\t${ctx.path()}\t" +
                            "${ctx.ip()}\t${ctx.contentLength()} bytes"

                log.info(requestLog)
            }

            after { ctx ->
                val headers = ctx.headerMap()
                headers["Content-Type"]?.let {
                    if (it.contains("application/json")) {
                        ctx.header("Content-Type", "application/json;charset=utf-8")
                    }
                }

            }
        }
    }

    private fun initExceptionHandlers() {
        with(server) {
            error(404) { ctx ->
                ctx.status(404)
            }

            exception(Exception::class.java) { ex, ctx ->
                log.info(ex.toString())
                when (ex) {
                    is BusinessException -> {
                        ctx.status(422)
                        ctx.json(BusinessErrorResponse(ex.message))
                    }
                    is com.fasterxml.jackson.databind.exc.MismatchedInputException ->
                        ctx.status(400)
                    else ->
                        ctx.status(400)

                }
            }
        }
    }

    private fun getOpenApiOptions(): OpenApiOptions {
        val applicationInfo = Info()
            .version("0.1")
            .description("rcmendes | Test :: Banking Transfer")

        return OpenApiOptions(applicationInfo).path("/banking-api")
            .swagger(SwaggerOptions("/api").title("Swagger Documentation of Banking API"))
    }
}