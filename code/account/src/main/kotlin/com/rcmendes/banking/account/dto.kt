package com.rcmendes.banking.account

import com.fasterxml.jackson.annotation.JsonProperty
import java.math.BigDecimal

/**
 * Represents the body data for a account creation request.
 * @param owner Name of the account owner.
 * @param amount Initial value to be deposited into account. Default value is 0.0
 * @param limit Limit of extra credit of the account. Default value is 0.0
 */
data class CreateAccountRequest(
    val owner: String,
    val amount: BigDecimal = BigDecimal.ZERO,
    val limit: BigDecimal = BigDecimal.ZERO
)

/**
 * Represents the body data for a account info response.
 * @param id Account ID (Number).
 * @param owner Name of the account owner.
 * @param balance Initial value to be deposited into account.
 * @param limit Limit of extra credit of the account.
 */
data class AccountInfoResponse(val id: String, val owner: String, val balance: BigDecimal, val limit: BigDecimal)

/**
 * Represents the body data for a money transfer into accounts transaction request.
 * @param fromAccountId Source account ID (Number).
 * @param toAccountId Target (destination) account ID (Number).
 * @param amount Amount of money to be transferred.
 */
data class TransferMoneyIntoAccountsRequest(
    val amount: BigDecimal,

    @JsonProperty(value = "to_account_id")
    val toAccountId: String
) {
    @JsonProperty(value = "from_account_id")
    var fromAccountId: String? = null
}

/**
 * Represents the body data for a account balance info response.
 * @param id Account ID (Number).
 * @param balance Initial value to be deposited into account.
 */
data class AccountBalanceResponse(val id: String, val balance: BigDecimal)

/**
 * Represents the body data for a business error info.
 * @param message Information about the business error
 */
data class BusinessErrorResponse(val message: String)