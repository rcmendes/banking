package com.rcmendes.banking.account

import java.math.BigDecimal
import java.util.*

/**
 * Handle all business logic related to the account management.
 * @param accountRepository Account repository manager.
 */
class AccountService(private val accountRepository: AccountRepository) {

    /**
     * Creates an new account.
     *
     * @param createAccount Data about the account to be created.
     *
     * @return Information of the created account.
     */
    fun createAccount(createAccount: CreateAccountRequest): AccountInfoResponse {
        if (createAccount.owner.isBlank()) {
            throw InsufficientCharactersLengthError("Owner", 5)
        }

        if (createAccount.amount < BigDecimal.ZERO) {
            throw NegativeValueError("Amount")
        }

        if (createAccount.limit < BigDecimal.ZERO) {
            throw NegativeValueError("Limit")
        }

        val account = this.accountRepository.insert(createAccount)
        return AccountInfoResponse(
            id = account.id.toString(),
            owner = account.owner,
            balance = account.balance,
            limit = account.limit
        )
    }

    /**
     * Transfer amount of money into accounts.
     *
     * @param transfer Transfer data information.
     *
     * @return The account balance of the source account.
     */
    fun transferIntoAccounts(transfer: TransferMoneyIntoAccountsRequest): AccountBalanceResponse {
        synchronized(this.accountRepository) {
            val fromId = transfer.fromAccountId ?: throw IllegalArgumentException("fromAccountId property was not set")
            val toId = transfer.toAccountId
            val amount = transfer.amount

            if (fromId.isBlank()) {
                throw InvalidAccountIdError()
            }

            if (toId.isBlank()) {
                throw InvalidAccountIdError()
            }

            var fromAccount = accountRepository.getById(UUID.fromString(fromId)) ?: throw AccountNotFoundError(fromId)
            var toAccount = accountRepository.getById(UUID.fromString(toId)) ?: throw AccountNotFoundError(toId)

            fromAccount.withdraw(amount)
            toAccount.deposit(amount)

            this.accountRepository.update(fromAccount)
            this.accountRepository.update(toAccount)

            return AccountBalanceResponse(id = fromId, balance = fromAccount.balance)
        }
    }

    /**
     * Lists all accounts.
     *
     * @return List of all accounts.
     */
    fun listAllAccounts(): List<AccountInfoResponse> {
        return accountRepository.getAll()
            .map {
                AccountInfoResponse(
                    id = it.id.toString(),
                    owner = it.owner,
                    balance = it.balance,
                    limit = it.limit
                )
            }
    }

    /**
     * Retrieves information of the specified account.
     * @param id Account ID (number) to be retrieved.
     * @return The account information if it is exists, null otherwise.
     */
    fun getAccountById(id: String): AccountInfoResponse? {
        if (id.isBlank()) {
            throw InvalidAccountIdError()
        }

        return accountRepository.getById(UUID.fromString(id))?.let {
            AccountInfoResponse(id = it.id.toString(), owner = it.owner, balance = it.balance, limit = it.limit)
        }
    }
}