package com.rcmendes.banking.cqrs.eventsourcing

import com.rcmendes.banking.cqrs.model.Account
import com.rcmendes.axiom.core.AggregateLifecycle
import com.rcmendes.axiom.core.handler.EventSourcingHandler

/**
 * Handles an event of an account created and apply it in the Account aggregate.
 */
class AccountCreatedEventSourcingHandler :
    EventSourcingHandler<AccountCreatedEvent, Account> {
    override fun on(event: AccountCreatedEvent, aggregateInstance: Account) {
        aggregateInstance.id = event.id
        aggregateInstance.owner = event.owner
        aggregateInstance.balance = event.amount
        aggregateInstance.limit = event.limit
    }
}

/**
 * Handles an event of an amount deposited into an account and apply it in the Account aggregate.
 */
class DepositedFromTransferEventSourcingHandler :
    EventSourcingHandler<DepositedEvent, Account> {
    override fun on(event: DepositedEvent, account: Account) {
        account.deposit(event.amount)
    }
}

/**
 * Handles an event of an amount withdrawn into an account and apply it in the Account aggregate.
 */
class WithdrawnFromTransferEventSourcingHandler :
    EventSourcingHandler<WithdrawnFromTransferEvent, Account> {
    override fun on(event: WithdrawnFromTransferEvent, account: Account) {
        account.withdraw(event.amount)

        AggregateLifecycle.handle(
            DepositedEvent(
                event.targetAccountId,
                event.amount
            ), account)
    }
}