package com.rcmendes.banking.cqrs.query

import java.math.BigDecimal
import java.time.Instant
import java.util.concurrent.ConcurrentHashMap

/**
 * Represents data to be stored about an account. (CQRS)
 */
data class AccountInfo(
    val accountId: String,
    val owner: String,
    var balance: BigDecimal,
    val limit: BigDecimal,
    var updatedAt: Instant = Instant.now()
) {

    fun deposit(amount: BigDecimal) {
        this.balance = this.balance.add(amount)
    }

    fun withdraw(amount: BigDecimal) {
        this.balance = this.balance.subtract(amount)
    }
}

/**
 * The Account info in-memory database.
 */
object AccountInfoRepositoryInMemory {
    private val accountsById = ConcurrentHashMap<String, AccountInfo>()

    fun save(account: AccountInfo) {
        accountsById[account.accountId] = account
    }

    fun getById(id: String) = accountsById[id]

    fun getAll() = accountsById.values.toList()
}
