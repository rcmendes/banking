package com.rcmendes.banking.account

import com.rcmendes.axiom.core.AggregateLifecycle
import com.rcmendes.axiom.core.gateway.CommandGateway
import com.rcmendes.axiom.core.gateway.QueryGateway
import com.rcmendes.banking.cqrs.command.CreateAccountCommand
import com.rcmendes.banking.cqrs.command.CreateAccountCommandHandler
import com.rcmendes.banking.cqrs.command.TransferCommandHandler
import com.rcmendes.banking.cqrs.command.TransferIntoAccountsCommand
import com.rcmendes.banking.cqrs.eventsourcing.AccountCreatedEventSourcingHandler
import com.rcmendes.banking.cqrs.eventsourcing.DepositedFromTransferEventSourcingHandler
import com.rcmendes.banking.cqrs.eventsourcing.WithdrawnFromTransferEventSourcingHandler
import com.rcmendes.banking.cqrs.model.Account
import com.rcmendes.banking.cqrs.query.*
import java.util.*

/**
 * Handle all business logic related to the account management.
 * @param accountRepository Account repository manager.
 */
class AccountService(private val accountRepository: AccountRepository) {

    init {
        loadHandlers()
    }

    /**
     * Creates an new account.
     *
     * @param createAccount Data about the account to be created.
     *
     * @return Information of the created account.
     */
    fun createAccount(createAccount: CreateAccountRequest): AccountInfoResponse {
        val id = UUID.randomUUID()
        CommandGateway.execute(
            CreateAccountCommand(
                id = id,
                owner = createAccount.owner,
                amount = createAccount.amount,
                limit = createAccount.limit
            )
        )


        val account =
            QueryGateway.execute<AccountInfo>(
                AccountInfoByIdQuery(
                    id
                )
            ) ?: throw AccountNotFoundError(id.toString())

        return AccountInfoResponse(
            id = account.accountId,
            owner = account.owner,
            balance = account.balance,
            limit = account.limit
        )
    }

    /**
     * Transfer amount of money into accounts.
     *
     * @param transfer Transfer data information.
     *
     * @return The account balance of the source account.
     */
    fun transferIntoAccounts(transfer: TransferMoneyIntoAccountsRequest): AccountBalanceResponse {
        synchronized(this.accountRepository) {
            val fromId = transfer.fromAccountId ?: throw InvalidAccountIdError()
            val toId = transfer.toAccountId
            val amount = transfer.amount

            if (fromId.isBlank()) {
                throw InvalidAccountIdError()
            }

            if (toId.isBlank()) {
                throw InvalidAccountIdError()
            }

            CommandGateway.execute(
                TransferIntoAccountsCommand(
                    fromAccountId = UUID.fromString(fromId),
                    amount = amount,
                    toAccountId = UUID.fromString(toId)
                )
            )

            val fromAccount = QueryGateway.execute<AccountInfo>(
                AccountInfoByIdQuery(
                    UUID.fromString(fromId)
                )
            )
                ?: throw AccountNotFoundError(fromId)

            return AccountBalanceResponse(id = fromId, balance = fromAccount.balance)
        }
    }

    /**
     * Lists all accounts.
     *
     * @return List of all accounts.
     */
    fun listAllAccounts(): List<AccountInfoResponse> {
        val list = QueryGateway.execute<List<AccountInfo>>(AllAccountInfoQuery()) ?: emptyList()
        return list
            .map {
                AccountInfoResponse(
                    id = it.accountId,
                    owner = it.owner,
                    balance = it.balance,
                    limit = it.limit
                )
            }
    }

    /**
     * Retrieves information of the specified account.
     * @param id Account ID (number) to be retrieved.
     * @return The account information if it is exists, null otherwise.
     */
    fun getAccountById(id: String): AccountInfoResponse? {
        if (id.isBlank()) {
            throw InvalidAccountIdError()
        }

        val account = QueryGateway.execute<AccountInfo>(
            AccountInfoByIdQuery(
                UUID.fromString(id)
            )
        )

        return account?.let {
            AccountInfoResponse(id = it.accountId, owner = it.owner, balance = it.balance, limit = it.limit)
        }
    }

    private fun loadHandlers() {
        //Commands
        CommandGateway.register(CreateAccountCommandHandler())
        CommandGateway.register(TransferCommandHandler())

        //Queries
        QueryGateway.register(ListAllAccountInfoQueryHandler())
        QueryGateway.register(AccountInfoByIdQueryHandler())

        //Event Handlers
        AggregateLifecycle.register(AccountCreatedEventHandler())
        AggregateLifecycle.register(DepositedEventHandler())
        AggregateLifecycle.register(WithdrawnEventHandler())

        // Event Sourcing Handlers
        AggregateLifecycle.register(AccountCreatedEventSourcingHandler())
        AggregateLifecycle.register(DepositedFromTransferEventSourcingHandler())
        AggregateLifecycle.register(WithdrawnFromTransferEventSourcingHandler())
    }
}