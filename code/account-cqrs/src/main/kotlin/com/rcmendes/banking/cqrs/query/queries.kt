package com.rcmendes.banking.cqrs.query

import com.rcmendes.axiom.core.modelling.Query
import java.util.*

/**
 * Represents an query used to fetch info of all accounts.
 */
class AllAccountInfoQuery() : Query

/**
 * Represents an query used to fetch info an specific account.
 * @param id ID of the Account.
 */
class AccountInfoByIdQuery(val id: UUID) : Query