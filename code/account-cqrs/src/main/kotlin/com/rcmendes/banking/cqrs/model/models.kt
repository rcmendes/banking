package com.rcmendes.banking.cqrs.model

import com.rcmendes.banking.account.InsufficientFundsError
import com.rcmendes.banking.account.InvalidAmountValueError
import com.rcmendes.axiom.core.modelling.Aggregate
import java.math.BigDecimal
import java.util.*

/**
 * Represents the Account aggregate.
 */
class Account() : Aggregate {
    var id: UUID? = null
    var owner: String? = null
    var balance: BigDecimal = BigDecimal.ZERO
    var limit: BigDecimal = BigDecimal.ZERO

    constructor(owner: String, balance: BigDecimal = BigDecimal.ZERO, limit: BigDecimal = BigDecimal.ZERO) : this() {
        this.owner = owner
        this.balance = balance
        this.limit = limit
    }

    constructor(
        id: UUID,
        owner: String,
        balance: BigDecimal = BigDecimal.ZERO,
        limit: BigDecimal = BigDecimal.ZERO
    ) : this(owner, balance, limit) {
        this.id = id
    }

    /**
     * Deposits a amount into account.
     * @param amount Amount to be deposited.
     */
    fun deposit(amount: BigDecimal) {
        if (amount <= BigDecimal.ZERO) {
            throw InvalidAmountValueError()
        }

        this.balance = this.balance.add(amount)
    }

    /**
     * Withdraw a amount into account.
     * @param amount Amount to be withdrawn.
     */
    fun withdraw(amount: BigDecimal) {
        if (amount <= BigDecimal.ZERO) {
            throw InvalidAmountValueError()
        }

        if (balance + limit < amount) {
            throw  InsufficientFundsError(this.id.toString())
        }

        this.balance = this.balance.subtract(amount)
    }

    override fun aggregateId(): Any? {
        return this.id
    }
}