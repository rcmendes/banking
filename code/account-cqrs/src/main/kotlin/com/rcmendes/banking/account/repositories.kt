package com.rcmendes.banking.account

import com.rcmendes.banking.cqrs.model.Account
import java.util.*
import java.util.concurrent.ConcurrentHashMap

interface AccountRepository {
    /**
     * Inserts an account into the repository.
     *
     * @param account Account to be stored.
     *
     * @return Account created
     */
    fun insert(account: CreateAccountRequest): Account

    /**
     * Fetch an specified account from the repository.
     * @param id Account ID (number) to be retrieved.
     *
     * @return The account if exists, null otherwise.
     */
    fun getById(id: UUID): Account?

    /**
     * Fetch all accounts from the repository.
     *
     * @return List of all accounts.
     */
    fun getAll(): List<Account>

    /**
     * Updates the account data into the repository.
     * @param account Account to be updated.
     */
    fun update(account: Account)
}

/**
 * Represents an account repository in memory.
 */
object AccountRepositoryInMemory : AccountRepository {
    private val accounts = ConcurrentHashMap<String, Account>()

    /**
     * @see AccountRepository.insert(CreateAccountRequest):Account
     */
    override fun insert(account: CreateAccountRequest): Account {
        val id = UUID.randomUUID()
        val newAccount = Account(
            owner = account.owner,
            balance = account.amount,
            limit = account.limit
        )
        newAccount.id = id
        accounts[id.toString()] = newAccount
        return newAccount
    }

    /**
     * @see AccountRepository.getById(UUID):Account
     */
    override fun getById(id: UUID): Account? {
        return accounts[id.toString()]
    }

    /**
     * @see AccountRepository.getAll():List<Account>
     */
    override fun getAll(): List<Account> {
        return accounts.values.toList()
    }

    /**
     * @see AccountRepository.update(Account)
     */
    override fun update(account: Account) {
        val id = account.id.toString()
        this.accounts.replace(id, account)
    }
}

