package com.rcmendes.banking.cqrs.query

import com.rcmendes.banking.account.AccountNotFoundError
import com.rcmendes.banking.cqrs.eventsourcing.AccountCreatedEvent
import com.rcmendes.banking.cqrs.eventsourcing.DepositedEvent
import com.rcmendes.banking.cqrs.eventsourcing.WithdrawnFromTransferEvent
import com.rcmendes.axiom.core.handler.EventHandler
import com.rcmendes.axiom.core.handler.QueryHandler
import java.time.Instant

private val repository = AccountInfoRepositoryInMemory

/**
 * Handles and account created event and stores into Account in-memory database.
 */
class AccountCreatedEventHandler : EventHandler<AccountCreatedEvent> {
    override fun on(event: AccountCreatedEvent) {
        val accountInfo = AccountInfo(
            accountId = event.id.toString(),
            owner = event.owner,
            balance = event.amount,
            limit = event.limit
        )
        AccountInfoRepositoryInMemory.save(accountInfo)
    }
}

/**
 * Handles and deposited amount into an account event and stores into Account in-memory database.
 */
class DepositedEventHandler : EventHandler<DepositedEvent> {
    override fun on(event: DepositedEvent) {
        val accountInfo = AccountInfoRepositoryInMemory.getById(event.id.toString())
            ?: throw AccountNotFoundError(event.id.toString())

        accountInfo.deposit(event.amount)
        accountInfo.updatedAt = Instant.now()

        AccountInfoRepositoryInMemory.save(accountInfo)
    }
}

/**
 * Handles and withdrawn amount from an account event and stores into Account in-memory database.
 */
class WithdrawnEventHandler : EventHandler<WithdrawnFromTransferEvent> {
    override fun on(event: WithdrawnFromTransferEvent) {
        val accountInfo = AccountInfoRepositoryInMemory.getById(event.id.toString())
            ?: throw AccountNotFoundError(event.id.toString())

        accountInfo.withdraw(event.amount)
        accountInfo.updatedAt = Instant.now()

        AccountInfoRepositoryInMemory.save(accountInfo)
    }
}

/**
 * Handles an query to fetch info of all accounts.
 */
class ListAllAccountInfoQueryHandler :
    QueryHandler<AllAccountInfoQuery, List<AccountInfo>> {
    override fun query(query: AllAccountInfoQuery) =
        AccountInfoRepositoryInMemory.getAll()
}

/**
 * Handles an query to fetch info of an specific account.
 */
class AccountInfoByIdQueryHandler : QueryHandler<AccountInfoByIdQuery, AccountInfo> {

    override fun query(query: AccountInfoByIdQuery) =
        AccountInfoRepositoryInMemory.getById(query.id.toString())
}