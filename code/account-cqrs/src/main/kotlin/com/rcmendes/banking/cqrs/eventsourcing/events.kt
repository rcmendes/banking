package com.rcmendes.banking.cqrs.eventsourcing

import com.rcmendes.axiom.core.modelling.Event
import java.math.BigDecimal
import java.util.*

/**
 * Represents the data of an account created event.
 */
data class AccountCreatedEvent(val id: UUID, val owner: String, val amount: BigDecimal, val limit: BigDecimal) :
    Event

/**
 * Represents the data of an amount deposited into an account.
 */
data class DepositedEvent(val id: UUID, val amount: BigDecimal) : Event

/**
 * Represents the data of an amount withdrawn from an account.
 */
data class WithdrawnFromTransferEvent(val id: UUID, val amount: BigDecimal, val targetAccountId: UUID) :
    Event