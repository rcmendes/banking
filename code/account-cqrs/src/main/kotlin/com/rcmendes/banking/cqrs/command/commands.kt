package com.rcmendes.banking.cqrs.command

import com.rcmendes.axiom.core.modelling.Command
import com.rcmendes.axiom.core.modelling.InitAggregateCommand
import java.math.BigDecimal
import java.util.*

/**
 * Represents the data of a creating account command.
 */
data class CreateAccountCommand(val id: UUID, val owner: String, val amount: BigDecimal, val limit: BigDecimal) :
    InitAggregateCommand {
    override fun aggregateId() = id.toString()
}

/**
 * Represents the data of a transfer into accounts command.
 */
data class TransferIntoAccountsCommand(val fromAccountId: UUID, val amount: BigDecimal, val toAccountId: UUID) :
    Command {
    override fun aggregateId() = fromAccountId.toString()
}