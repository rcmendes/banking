package com.rcmendes.banking.cqrs.command

import com.rcmendes.banking.account.InsufficientCharactersLengthError
import com.rcmendes.banking.account.NegativeValueError
import com.rcmendes.banking.cqrs.eventsourcing.AccountCreatedEvent
import com.rcmendes.banking.cqrs.eventsourcing.WithdrawnFromTransferEvent
import com.rcmendes.banking.cqrs.model.Account
import com.rcmendes.axiom.core.AggregateLifecycle
import com.rcmendes.axiom.core.handler.CommandHandler
import java.math.BigDecimal
import java.util.*

/**
 * Command responsible for handling creation of an aggregate Account.
 */
class CreateAccountCommandHandler : CommandHandler<CreateAccountCommand, Account> {
    override fun send(command: CreateAccountCommand, account: Account) {
        if (command.owner.isBlank()) {
            throw InsufficientCharactersLengthError("Owner", 5)
        }

        if (command.amount < BigDecimal.ZERO) {
            throw NegativeValueError("Amount")
        }

        if (command.limit < BigDecimal.ZERO) {
            throw NegativeValueError("Limit")
        }

        AggregateLifecycle.handle(
            AccountCreatedEvent(
                id = command.id,
                owner = command.owner,
                amount = command.amount,
                limit = command.limit
            ), account
        )
    }
}

/**
 * Command responsible for handling a transfer transaction operation of an amount into Accounts.
 */
class TransferCommandHandler :
    CommandHandler<TransferIntoAccountsCommand, Account> {
    override fun send(command: TransferIntoAccountsCommand, account: Account) {
        AggregateLifecycle.handle(
            WithdrawnFromTransferEvent(
                command.fromAccountId,
                command.amount,
                command.toAccountId
            ), account
        )
    }
}
