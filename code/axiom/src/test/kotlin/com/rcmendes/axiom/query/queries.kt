package com.rcmendes.axiom.query

import com.rcmendes.axiom.core.modelling.Query
import java.time.Instant
import java.util.concurrent.ConcurrentHashMap

data class AccountView(
    val accountId: Int,
    val owner: String,
    var balance: Double,
    var updatedAt: Instant = Instant.now()
) {

    fun deposit(amount: Double) {
        this.balance += amount
    }

    fun withdraw(amount: Double) {
        this.balance -= amount
    }
}

object AccountRepositoryInMemory {
    private val accountsById = ConcurrentHashMap<Int, AccountView>()

    fun save(account: AccountView) {
        accountsById.put(account.accountId, account)
    }

    fun getById(id: Int) = accountsById[id]

    fun getAll() = accountsById.values.toList()
}

class AllAccountViewsQuery() : Query

class AccountViewByIdQuery(val id: Int) : Query