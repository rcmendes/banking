package com.rcmendes.axiom.eventsourcing

import com.rcmendes.axiom.core.AggregateLifecycle
import com.rcmendes.axiom.core.handler.EventSourcingHandler
import com.rcmendes.axiom.model.Account

class AccountCreatedEventSourcingHandler :
    EventSourcingHandler<AccountCreatedEvent, Account> {
    override fun on(event: AccountCreatedEvent, aggregateInstance: Account) {
        aggregateInstance.id = event.id
        aggregateInstance.owner = event.owner
        aggregateInstance.balance = event.amount

        println("Account created. Info: $aggregateInstance")
    }
}

class DepositedFromTransferEventSourcingHandler :
    EventSourcingHandler<DepositedEvent, Account> {
    override fun on(event: DepositedEvent, account: Account) {
        account.balance = account.balance + event.amount

        println("Deposited ${event.amount} into Account ID: ${account.id}")
    }
}

class WithdrawnFromTransferEventSourcingHandler :
    EventSourcingHandler<WithdrawnFromTransferEvent, Account> {
    override fun on(event: WithdrawnFromTransferEvent, account: Account) {
        val balance = account.balance - event.amount

        if (balance < 0) {
            throw Exception(
                "Insufficient funds. Account ID: ${account.id} | " +
                        "Balance: ${account.balance} | Required: ${event.amount}"
            )
        }

        account.balance = balance

        println(
            "Withdrawn ${event.amount}\tFROM Account ID: ${account.id}\t" +
                    "TARGET Account ID: ${event.targetAccountId}"
        )

        AggregateLifecycle.handle(
            DepositedEvent(
                event.targetAccountId,
                event.amount
            ), account
        )
    }
}