package com.rcmendes.axiom

import com.rcmendes.axiom.command.CreateAccountCommand
import com.rcmendes.axiom.command.CreateAccountCommandHandler
import com.rcmendes.axiom.command.TransferCommandHandler
import com.rcmendes.axiom.command.TransferIntoAccountsCommand
import com.rcmendes.axiom.core.AggregateLifecycle
import com.rcmendes.axiom.core.EventLifecycle
import com.rcmendes.axiom.core.gateway.CommandGateway
import com.rcmendes.axiom.core.gateway.QueryGateway
import com.rcmendes.axiom.eventsourcing.AccountCreatedEventSourcingHandler
import com.rcmendes.axiom.eventsourcing.DepositedFromTransferEventSourcingHandler
import com.rcmendes.axiom.eventsourcing.WithdrawnFromTransferEventSourcingHandler
import com.rcmendes.axiom.model.Account
import com.rcmendes.axiom.query.*

fun main(args: Array<String>) {
    loadHandlers()

    CommandGateway.execute(CreateAccountCommand("Owner 1", 100.0))
    CommandGateway.execute(CreateAccountCommand("Owner 2", 200.0))

    val list = QueryGateway.execute<List<AccountView>>(AllAccountViewsQuery())!!
    var account1 = list[0]
    var account2 = list[1]

    CommandGateway.execute(
        TransferIntoAccountsCommand(
            account2.accountId,
            50.0,
            toAccountId = account1.accountId
        )
    )

    account1 = QueryGateway.execute<AccountView>(
        AccountViewByIdQuery(
            account1.accountId
        )
    )!!
    account2 = QueryGateway.execute<AccountView>(
        AccountViewByIdQuery(
            account2.accountId
        )
    )!!

    println("Account [${account1.accountId}]: $account1")
    println("Account [${account2.accountId}]: $account2")

    println("Events:")
    EventLifecycle.allEvents().forEach {
        println("$it")
    }
}

fun loadHandlers() {
    //Commands
    CommandGateway.register(CreateAccountCommandHandler(), Account::class)
    CommandGateway.register(TransferCommandHandler(), Account::class)

    //Queries
    QueryGateway.register(ListAllAccountViewsQueryHandler())
    QueryGateway.register(AccountViewByIdQueryHandler())

    //Event Handlers
    AggregateLifecycle.register(AccountCreatedEventHandler())
    AggregateLifecycle.register(DepositedEventHandler())
    AggregateLifecycle.register(WithdrawnEventHandler())

    // Event Sourcing Handlers
    AggregateLifecycle.register(AccountCreatedEventSourcingHandler())
    AggregateLifecycle.register(DepositedFromTransferEventSourcingHandler())
    AggregateLifecycle.register(WithdrawnFromTransferEventSourcingHandler())
}
