package com.rcmendes.axiom.command

import com.rcmendes.axiom.core.AggregateLifecycle
import com.rcmendes.axiom.core.error.AggregateUndefinedError
import com.rcmendes.axiom.core.handler.CommandHandler
import com.rcmendes.axiom.eventsourcing.AccountCreatedEvent
import com.rcmendes.axiom.eventsourcing.WithdrawnFromTransferEvent
import com.rcmendes.axiom.model.Account

class CreateAccountCommandHandler : CommandHandler<CreateAccountCommand, Account> {
    companion object {
        var counterId = 1
    }

    override fun send(command: CreateAccountCommand, account: Account) {
        AggregateLifecycle.handle(
            AccountCreatedEvent(
                counterId++,
                command.owner,
                command.amount
            ), account
        )
    }
}

class TransferCommandHandler :
    CommandHandler<TransferIntoAccountsCommand, Account> {
    override fun send(command: TransferIntoAccountsCommand, account: Account) {
        AggregateLifecycle.handle(
            WithdrawnFromTransferEvent(
                command.fromAccountId,
                command.amount,
                command.toAccountId
            ), account
        )
    }
}
