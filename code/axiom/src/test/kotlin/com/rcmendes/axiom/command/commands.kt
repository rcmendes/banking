package com.rcmendes.axiom.command

import com.rcmendes.axiom.core.modelling.Command
import com.rcmendes.axiom.core.modelling.InitAggregateCommand

data class CreateAccountCommand(val owner: String, val amount: Double) :
    InitAggregateCommand {
    override fun aggregateId(): String? = null
}
data class DepositMoneyFromTransferCommand(val toAccountId: Int, val amount: Double, val fromAccountId: Int) :
    Command {
    override fun aggregateId()= toAccountId.toString()
}
data class TransferIntoAccountsCommand(val fromAccountId: Int, val amount: Double, val toAccountId: Int) :
    Command {
    override fun aggregateId()= fromAccountId.toString()
}