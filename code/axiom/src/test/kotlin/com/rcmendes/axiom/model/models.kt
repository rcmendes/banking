package com.rcmendes.axiom.model

import com.rcmendes.axiom.core.modelling.Aggregate

class Account() : Aggregate {
    var id: Int? = null
    var owner: String? = null
    var balance: Double = 0.0

    constructor(id: Int, owner: String, balance: Double) : this() {
        this.id = id
        this.owner = owner
        this.balance = balance
    }

    override fun aggregateId(): Any? {
        return id
    }

    override fun toString(): String {
        return "id=$id, owner=$owner, balance=$balance"
    }
}