package com.rcmendes.axiom.query

import com.rcmendes.axiom.core.handler.EventHandler
import com.rcmendes.axiom.core.handler.QueryHandler
import com.rcmendes.axiom.eventsourcing.AccountCreatedEvent
import com.rcmendes.axiom.eventsourcing.DepositedEvent
import com.rcmendes.axiom.eventsourcing.WithdrawnFromTransferEvent
import java.time.Instant

class AccountCreatedEventHandler() : EventHandler<AccountCreatedEvent> {
    private val repository = AccountRepositoryInMemory
    override fun on(event: AccountCreatedEvent) {
        val accountView = AccountView(event.id, event.owner, event.amount)
        AccountRepositoryInMemory.save(accountView)
    }
}

class DepositedEventHandler() : EventHandler<DepositedEvent> {
    private val repository = AccountRepositoryInMemory
    override fun on(event: DepositedEvent) {
        val accountView = AccountRepositoryInMemory.getById(event.id)
            ?: throw IllegalArgumentException("Account number ${event.id} was not found.")

        accountView.deposit(event.amount)
        accountView.updatedAt = Instant.now()

        AccountRepositoryInMemory.save(accountView)
    }
}

class WithdrawnEventHandler() : EventHandler<WithdrawnFromTransferEvent> {
    private val repository = AccountRepositoryInMemory
    override fun on(event: WithdrawnFromTransferEvent) {
        val accountView = AccountRepositoryInMemory.getById(event.id)
            ?: throw IllegalArgumentException("Account number ${event.id} was not found.")

        accountView.withdraw(event.amount)
        accountView.updatedAt = Instant.now()

        AccountRepositoryInMemory.save(accountView)
    }
}

class ListAllAccountViewsQueryHandler() :
    QueryHandler<AllAccountViewsQuery, List<AccountView>> {
    private val repository = AccountRepositoryInMemory

    override fun query(query: AllAccountViewsQuery) =
        AccountRepositoryInMemory.getAll()
}

class AccountViewByIdQueryHandler() :
    QueryHandler<AccountViewByIdQuery, AccountView> {
    private val repository = AccountRepositoryInMemory

    override fun query(query: AccountViewByIdQuery) =
        AccountRepositoryInMemory.getById(query.id)
}