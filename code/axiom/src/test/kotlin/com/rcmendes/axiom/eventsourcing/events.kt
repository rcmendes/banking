package com.rcmendes.axiom.eventsourcing

import com.rcmendes.axiom.core.modelling.Event

data class AccountCreatedEvent(val id: Int, val owner: String, val amount: Double) :
    Event
data class DepositedEvent(val id: Int, val amount: Double) : Event
data class WithdrawnFromTransferEvent(val id: Int, val amount: Double, val targetAccountId: Int) :
    Event
