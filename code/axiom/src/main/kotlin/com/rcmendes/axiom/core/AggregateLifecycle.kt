package com.rcmendes.axiom.core

import com.rcmendes.axiom.core.error.AggregateNotFoundError
import com.rcmendes.axiom.core.handler.EventHandler
import com.rcmendes.axiom.core.handler.EventSourcingHandler
import com.rcmendes.axiom.core.modelling.Aggregate
import com.rcmendes.axiom.core.modelling.Event
import org.slf4j.LoggerFactory
import java.time.Instant
import java.util.concurrent.ConcurrentHashMap
import kotlin.reflect.KClass
import kotlin.reflect.KType
import kotlin.reflect.full.createInstance
import kotlin.reflect.full.declaredFunctions
import kotlin.reflect.full.starProjectedType

object AggregateLifecycle {
    private val log = LoggerFactory.getLogger(this::class.java)

    private val aggregatesById = ConcurrentHashMap<String, Aggregate>()

    private val eventSourcingHandlerForEvent = mutableMapOf<KType, EventSourcingHandler<Event, Aggregate>>()
    private val eventHandlerForEvent = mutableMapOf<KType, EventHandler<Event>>()
    private val eventsByAggregateId = mutableMapOf<Any, MutableList<DataEvent>>()

    fun <E : Event, A : Aggregate> register(handler: EventSourcingHandler<E, A>) {
        handler as EventSourcingHandler<Event, Aggregate>

        val clazz = handler::class
        val members = clazz.declaredFunctions
        members.forEach { member ->
            member.parameters.forEach { parameter ->
                parameter.name?.let {
                    eventSourcingHandlerForEvent.putIfAbsent(parameter.type, handler)
                }
            }
        }
    }

    fun <E : Event> register(handler: EventHandler<E>) {
        handler as EventHandler<Event>

        val clazz = handler::class
        val members = clazz.declaredFunctions
        members.forEach { member ->
            member.parameters.forEach { parameter ->
                parameter.name?.let {
                    eventHandlerForEvent.putIfAbsent(parameter.type, handler)
                }
            }
        }
    }

    fun handle(event: Event, aggregate: Aggregate) {
        val eventType = event::class.starProjectedType
        val eventSourcingHandlerApplicable =
            eventSourcingHandlerForEvent[eventType]
                ?: throw IllegalArgumentException("No EventSourcingHandler for $eventType was set")

        val eventHandlerApplicable =
            eventHandlerForEvent[eventType]
                ?: throw IllegalArgumentException("No EventHandler for $eventType was set")

        log.info("[${event::class.qualifiedName}] Applying event: $event")

        eventSourcingHandlerApplicable.on(event, aggregate)
        eventHandlerApplicable.on(event)

        val aggregateId = aggregate.aggregateId()
        if (aggregateId != null) {
            aggregatesById.putIfAbsent(aggregateId.toString(), aggregate)
        }

        EventLifecycle.store(aggregateId.toString(), event)
    }

    fun <T : Aggregate> aggregateByIdFromEvents(id: Any, clazz: KClass<T>): Aggregate {
        val aggregate = clazz.createInstance()
        val events = eventsByAggregateId[id] ?: throw AggregateNotFoundError(
            id.toString()
        )

        events.sortBy { it.timestamp }
        events.forEach { dataEvent ->
            val event = dataEvent.event
            val handler = eventSourcingHandlerForEvent[event::class.starProjectedType]
            handler?.let {
                handler.on(event, aggregate)
            }
        }

        return aggregate
    }

    inline fun <reified T : Aggregate> aggregateByIdFromEvents(id: Any): Aggregate {
        return aggregateByIdFromEvents(id, T::class)
    }

    fun <ID> getAggregateById(id: ID) = aggregatesById[id.toString()]

    data class DataEvent(val event: Event, val timestamp: Instant = Instant.now())
}