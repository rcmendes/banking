package com.rcmendes.axiom.core.modelling

//TODO Replace Any for Generics

interface Command {
    fun aggregateId(): String?
}

interface InitAggregateCommand: Command

interface Query

interface Event

interface Aggregate {
    fun aggregateId(): Any?
}