package com.rcmendes.axiom.core

import com.rcmendes.axiom.core.modelling.Event
import java.time.Instant
import java.util.concurrent.ConcurrentLinkedDeque

// TODO Enable use others persistence Database.

object EventLifecycle {
    private val events = ConcurrentLinkedDeque<EventData>()

    fun store(aggregateId: String, event: Event) {
        events.add(EventData(aggregateId, event))
    }

    fun eventsFromAggregateById(id: String, since: Instant? = null) = events.filter { evt ->
        if (since != null) {
            (since == evt.timestamp || since.isAfter(evt.timestamp)) && evt.aggregateId == id
        } else {
            evt.aggregateId == id
        }
    }

    fun allEvents() = events.toList()
}

data class EventData(val aggregateId: String, val event: Event, val timestamp: Instant = Instant.now())