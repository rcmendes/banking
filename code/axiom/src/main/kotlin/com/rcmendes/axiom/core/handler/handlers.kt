package com.rcmendes.axiom.core.handler

import com.rcmendes.axiom.core.modelling.Aggregate
import com.rcmendes.axiom.core.modelling.Command
import com.rcmendes.axiom.core.modelling.Event
import com.rcmendes.axiom.core.modelling.Query

/**
 * Handles event sourcing (Events may be applied in the Aggregate).
 *
 * @param E An implementation class of the Event interface.
 * @param A An implementation class of the Aggregate interface.
 */
interface EventSourcingHandler<E : Event, A : Aggregate> {

    /**
     * Performs the event.
     *
     * @param event An Event instance.
     * @param aggregate An Aggregate instance.
     */
    fun on(event: E, aggregate: A)
}

/**
 * Handles events. General is used in CQRS (Command Query Responsibility Segregation) pattern.
 *
 * @param E An implementation class of the Event interface.
 */
interface EventHandler<E : Event> {
    /**
     * Performs the event.
     *
     * @param event An Event instance.
     */
    fun on(event: E)
}

/**
 * Handles Command events within the Aggregate.
 *
 * @param C An implementation class of the Command interface.
 * @param A An implementation class of the Aggregate interface.
 */
interface CommandHandler<C : Command, A : Aggregate> {

    /**
     * Executes the command.
     *
     * @param command A Command instance.
     * @param aggregate An Aggregate instance.
     */
    fun send(command: C, aggregate: A)
}

/**
 * Handles Query events.
 *
 * @param Q An implementation class of the Query interface.
 * @param R Return type of the query.
 */
interface QueryHandler<Q : Query, R> {

    /**
     * Executes the query.
     *
     * @param query a query instance.
     * @return R Return of the query.
     */
    fun query(query: Q): R?
}
