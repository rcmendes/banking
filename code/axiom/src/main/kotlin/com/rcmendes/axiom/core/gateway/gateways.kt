package com.rcmendes.axiom.core.gateway

import com.rcmendes.axiom.core.AggregateLifecycle
import com.rcmendes.axiom.core.error.AggregateNotFoundError
import com.rcmendes.axiom.core.error.AggregateUndefinedError
import com.rcmendes.axiom.core.handler.CommandHandler
import com.rcmendes.axiom.core.handler.QueryHandler
import com.rcmendes.axiom.core.modelling.Aggregate
import com.rcmendes.axiom.core.modelling.Command
import com.rcmendes.axiom.core.modelling.InitAggregateCommand
import com.rcmendes.axiom.core.modelling.Query
import org.slf4j.LoggerFactory
import kotlin.reflect.KClass
import kotlin.reflect.KType
import kotlin.reflect.full.createInstance
import kotlin.reflect.full.declaredFunctions
import kotlin.reflect.full.starProjectedType

object CommandGateway {
    private val log = LoggerFactory.getLogger(this::class.java)
    private val handlerForCommand = mutableMapOf<KType, CommandHandler<Command, Aggregate>>()
    private val aggregateClassForCommandHandler = mutableMapOf<String, KClass<Aggregate>>()

    inline fun <C : Command, reified A : Aggregate> register(handler: CommandHandler<C, A>) {
        register(handler, A::class)
    }


    fun <C : Command, A : Aggregate> register(handler: CommandHandler<C, A>, aggregateClazz: KClass<A>) {
        handler as CommandHandler<Command, Aggregate>
        aggregateClazz as KClass<Aggregate>

        val clazz = handler::class
        val members = clazz.declaredFunctions
        members.forEach { member ->
            member.parameters.forEach { parameter ->
                parameter.name?.let {
                    handlerForCommand.putIfAbsent(parameter.type, handler)
                }
            }
        }

        val key = handler::class.qualifiedName!!
        aggregateClassForCommandHandler.putIfAbsent(key, aggregateClazz)
    }

    fun execute(command: Command) {
        val commandType = command::class.starProjectedType
        val handlerApplicableForCommand =
            handlerForCommand[commandType]
                ?: throw IllegalArgumentException("No CommandHandler for $commandType was set")

        log.info("[${command::class.qualifiedName}] Executing Command: $command")

        val aggregateId = command.aggregateId()

        val aggregate = when {
            command is InitAggregateCommand -> {
                val key = handlerApplicableForCommand::class.qualifiedName!!
                val aggregateClass = aggregateClassForCommandHandler[key]
                aggregateClass!!.createInstance()
            }
            aggregateId == null -> throw AggregateUndefinedError()
            else -> {
                val id = aggregateId!!
                AggregateLifecycle.getAggregateById(id) ?: throw AggregateNotFoundError(
                    id
                )
            }
        }

        handlerApplicableForCommand.send(command, aggregate)
    }
}

object QueryGateway {
    private val log = LoggerFactory.getLogger(this::class.java)

    private val handlerForQuery = mutableMapOf<KType, QueryHandler<Query, Any?>>()

    fun <Q : Query, R : Any?> register(handler: QueryHandler<Q, R>) {
        handler as QueryHandler<Query, Any?>

        val clazz = handler::class
        val members = clazz.declaredFunctions
        members.forEach { member ->
            member.parameters.forEach { parameter ->
                parameter.name?.let {
                    handlerForQuery.putIfAbsent(parameter.type, handler)
                }
            }
        }
    }

    fun <R : Any?> execute(query: Query): R? {
        val queryType = query::class.starProjectedType
        val handlerApplicableForQuery =
            handlerForQuery[queryType]
                ?: throw IllegalArgumentException("No QueryHandler for $queryType was set")

        log.info("[${query::class.qualifiedName}] Executing Query: $query")

        return handlerApplicableForQuery.query(query) as R?
    }
}