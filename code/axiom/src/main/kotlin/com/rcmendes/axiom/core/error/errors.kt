package com.rcmendes.axiom.core.error

sealed class AxiomError(override val message: String) : Exception(message)

class AggregateUndefinedError() : AxiomError("Aggregate is undefined")

class AggregateNotFoundError(private val id: String) : AxiomError("Aggregate with ID: $id was not found") {
    fun aggregateId() = id
}