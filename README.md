# Banking account transfer:
>Comparing solutions with and without cqrs and event sourcing patterns.

Banking Transfer API applications using:
- Coroutines
- Javalin
- Concurrence
- CQRS and Event Sourcing patterns.

This repository contains 2 proposed solutions and 1 library for Event Sourcing and CQRS.

The structure of the root folder is the following:

-   **builds**: contains the jar files (executable) of the solutions;
-   **banking**: contains the code of the 2 proposed solutions and the axiom library.

## builds folder

> The executable files of the 2 proposed solutions.

I've created jar executable bundles for simplification.

To run the account solution, without Axiom, just execute:

-   Default port (_8080_): **_java -jar account-0.1-SNAPSHOT.jar_**
-   Specific port: **_java -jar account-0.1-SNAPSHOT.jar port=_**[*PORT*]

To run the account solution with Axiom, execute:

-   Default port (_8080_): **_java -jar account-cqrs-0.1-SNAPSHOT.jar_**
-   Specific port: **_java -jar account-cqrs-0.1-SNAPSHOT.jar port=_**[*PORT*]

## banking folder

> The code of the 2 proposed solutions and also of the Axiom library.

The code is distributed as the following:

### account

> Contains the code implementing an MVC strategy.

**Future work**: Add mockito library or similar to generate mocks in tests.

### account-cqrs

> Contains the code implementing an Event Sourcing and CQRS strategy.

I used the axiom library to provide Event Sourcing and CQRS features.

I recommend checking it out this one too! ;)

**Future work**: Add mockito library or similar to generate mocks in tests.

### axiom

> **Axiom**: Simple Even Sourcing and CQRS (Command Query Responsibility Segregation) implementation for Kotlin

Since these patterns seem to be one of the most used in real life,
I created this library to meets the challenge requirements applying an elegant design pattern.

**Future work**: Unfortunately, I didn't have time to document all the code and make some improvements, aiming flexibility and better test code coverage.

#### Documentation of the API (Swagger)

> Both presented solutions has Swagger API support. It can ber accessed from the path **/api**. e.g.: http://localhost:8080/api

#### Technical considerations

-   Programming language: **Kotlin**
-   Main libraries used:
    -   Javalin
    -   Jackson
    -   Coroutines
    -   JUnit 5
    -   Rest Assured
    -   OpenAPI
    -   Swagger

> For the sake of simplicity  (e.g.: in-memory solution), the data is consistent only per JVM instance 
>and no hexagonal or clean architecture project structure was applied.
